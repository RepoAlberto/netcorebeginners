﻿using CarDealer.SeedWork.Infraestructure;
using System.ComponentModel.DataAnnotations;

namespace CarDealer.Api
{
    public class NewDealer
    {
        [MaxLength(Constants.NAME_DEFAULT_LENGTH, ErrorMessage = "Name of the dealer exceeds from maximun length")]
        public string Name { get; set; }

        [EmailAddress(ErrorMessage = "Email is not in the correct format. Example: mydealer@dealer.es")]
        public string Email { get; set; }

        [MaxLength(5, ErrorMessage = "Postal code exceed from max length")]
        public string PostalCode { get; set; }
    }
}