﻿namespace CarDealer.Api
{
    public class DealerResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PostalCode { get; set; }
        public int Rating { get; set; }
    }
}