﻿using AutoMapper;
using CarDealer.Domain.Models.CarDealer;

namespace CarDealer.Api
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            DealerMappings();
            CarMappings();
        }

        private void CarMappings()
        {
            CreateMap<Car, CarResponse>()
                .ForMember(dest => dest.NumberOfWheels, opt => opt.MapFrom(src => src.Wheels));

            CreateMap<NewCar, Car>();

            CreateMap<UpdateCar, Car>()
                .ForMember(e => e.DealerId, opts => opts.Ignore())
                .ForMember(e => e.Id, opts => opts.Ignore());


            CreateMap<Car, UpdateCar>();
        }

        private void DealerMappings()
        {
            CreateMap<Dealer, DealerResponse>();

            CreateMap<NewDealer, Dealer>()
                .ForMember(e => e.Id, opt => opt.Ignore());

            CreateMap<PutDealer, Dealer>()
                .ForMember(e => e.Id, opt => opt.Ignore());

            CreateMap<Dealer, PutDealer>();
        }
    }
}
