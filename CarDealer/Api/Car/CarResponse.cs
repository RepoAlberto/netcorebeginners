﻿namespace CarDealer.Api
{
    public class CarResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int NumberOfWheels { get; set; }
    }
}