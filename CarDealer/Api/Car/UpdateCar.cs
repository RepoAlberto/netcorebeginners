﻿using CarDealer.SeedWork.Infraestructure;
using System.ComponentModel.DataAnnotations;

namespace CarDealer.Api
{
    public class UpdateCar
    {
        [MaxLength(Constants.MAX_DEFAULT_LENGTH), Required]
        public string Name { get; set; }
    }
}
