﻿using CarDealer.SeedWork.Infraestructure;
using System.ComponentModel.DataAnnotations;

namespace CarDealer.Api
{
    public class NewCar
    {
        public int Id { get; set; }

        [MaxLength(Constants.MAX_DEFAULT_LENGTH), Required]
        public string Name { get; set; }
    }
}
