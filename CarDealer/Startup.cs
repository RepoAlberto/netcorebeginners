using AutoMapper;
using CarDealer.Api;
using CarDealer.Domain.Interfaces;
using CarDealer.Infraestructure;
using CarDealer.Infraestructure.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using System;
using System.Linq;

namespace CarDealer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //{"key": "Accept", "value": "application\xml"}. La de por defecto: {"key": "Accept", "value": "application\json"}

            services
                .AddDbContext<CarDealerDbContext>(opt => opt.UseSqlServer(this.Configuration["connectionStrings"]))
                .AddAutoMapper(mapper => mapper.AddProfile(typeof(GeneralProfile)))
                .AddSwaggerGen(options =>
                {
                    options.SwaggerDoc("v1", new OpenApiInfo
                    {
                        Version = "V0",
                        Title = "Car Dealer API",
                        Description = "Car Dealer RestFull API (ASP.NET Core 3.1)",
                        Contact = new OpenApiContact()
                        {
                            Name = "Swagger Codegen Contributors",
                            Url = new Uri("https://github.com/swagger-api/swagger-codegen"),
                            Email = ""
                        },
                    });

                    options.DescribeAllParametersInCamelCase();
                    options.CustomSchemaIds(t => t.FullName);
                    options.IncludeXmlComments($"{AppContext.BaseDirectory}CarDealer.xml");
                })
                .AddTransient<IDealerRepository, DealerRepository>()
                .AddTransient<ICarRepository, CarRepository>()
                .AddControllers(setupAction => setupAction.ReturnHttpNotAcceptable = true)
                .AddNewtonsoftJson(setupAction => setupAction.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver())
                .AddXmlDataContractSerializerFormatters();
                //.AddMvcOptions(opts => opts.EnableEndpointRouting = false);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger()
               .UseSwaggerUI(c =>
               {
                   c.SwaggerEndpoint("/swagger/v1/swagger.json", "Car Dealer");
               });

            //app.UseMvc(route =>
            //{
            //    route.MapRoute(name: "dealer",
            //                   template: "{controller}/{action}/{id?}",
            //                   defaults: new { controller = "Dealer", action = "GetDealer" });

            //    //route.MapRoute("Paginated-Dealer", "{controller=Dealer}/{action=PaginatedDealer}/{take?}/{skip?}");
            //});

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                //endpoints.MapControllerRoute(name: "default",
                //                pattern: "{controller=Dealer}/{action=GetAllDealers}/{id?}");
            });
        }
    }
}
