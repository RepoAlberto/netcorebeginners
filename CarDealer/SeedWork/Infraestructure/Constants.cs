﻿namespace CarDealer.SeedWork.Infraestructure
{
    public static class Constants
    {
        public const int MAX_DEFAULT_LENGTH = 250;

        public const int NAME_DEFAULT_LENGTH = 50;
    }
}
