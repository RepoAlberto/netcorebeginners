﻿using AutoMapper;
using CarDealer.Api;
using CarDealer.Domain.Interfaces;
using CarDealer.Domain.Models.CarDealer;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Message = CarDealer.SeedWork.Api.Constants;

namespace CarDealer.Controllers
{
    //[Route("")]
    [Route("/api/dealer/")]
    public class DealerController : ControllerBase
    {
        private readonly IDealerRepository dealerRepository;
        private readonly IMapper mapper;

        public DealerController(IDealerRepository dealerRepository, IMapper mapper)
        {
            this.dealerRepository = dealerRepository;
            this.mapper = mapper;
        }

        [HttpGet("")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(List<DealerResponse>))]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ProblemDetails))]
        public async Task<IActionResult> GetAllDealers(CancellationToken cancellationToken)
        {
            var dealer = await this.dealerRepository.GetAllPaginatedAsync(100000, 0, cancellationToken);

            return Ok(dealer);
        }

        [HttpGet("{id}", Name = "GetDealer")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(DealerResponse))]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ProblemDetails))]
        public async Task<IActionResult> GetOneDealer(int id, CancellationToken cancellationToken)
        {
            var dealer = await this.dealerRepository.GetOneByIdAsync(id, cancellationToken);

            if (dealer == null)
                return NotFound();

            var dto = this.mapper.Map<DealerResponse>(dealer);

            return Ok(dto);
        }

        [HttpGet("{take}/{skip}", Name = "PaginatedDealer")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(List<DealerResponse>))]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ProblemDetails))]
        public async Task<IActionResult> GetDealersPaginated(int take, int skip, CancellationToken cancellationToken)
        {
            var dealer = await this.dealerRepository.GetAllPaginatedAsync(take, skip, cancellationToken);

            return Ok(dealer);
        }

        [HttpPost("")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ProblemDetails))]
        public async Task<IActionResult> CreateNewDealer([FromBody]NewDealer newDealer, CancellationToken cancellationToken)
        {
            if (newDealer == null)
                return BadRequest();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var dealer = this.mapper.Map<Dealer>(newDealer);
            await this.dealerRepository.CreateAsync(dealer, cancellationToken);

            if (!await this.dealerRepository.SaveAsync(cancellationToken))
                return StatusCode(500, Message.STATUS_CODE_500_GENERAL_MESSAGE);

            return CreatedAtRoute("GetDealer", new { id = dealer.Id }, dealer);
        }

        [HttpPut("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ProblemDetails))]
        public async Task<IActionResult> UpdateFullDealer(int id, [FromBody]PutDealer newDealer, CancellationToken cancellationToken)
        {
            if (newDealer == null)
                return BadRequest();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var dealer = await this.dealerRepository.GetOneByIdAsync(id, cancellationToken);

            if (dealer == null)
                return NotFound();

            this.mapper.Map<PutDealer, Dealer>(newDealer, dealer);

            if (!await this.dealerRepository.SaveAsync(cancellationToken))
                return StatusCode(500, Message.STATUS_CODE_500_GENERAL_MESSAGE);

            return NoContent();
        }

        [HttpPatch("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ProblemDetails))]
        public async Task<IActionResult> UpdateSomeDealer(int id, [FromBody]JsonPatchDocument<PutDealer> patchDealerDocument, CancellationToken cancellationToken)
        {
            if (patchDealerDocument == null)
                return BadRequest();

            var dealer = await this.dealerRepository.GetOneByIdAsync(id, cancellationToken);

            if (dealer == null)
                return NotFound();

            var dto = mapper.Map<PutDealer>(dealer);
            patchDealerDocument.ApplyTo(dto);

            //if (!ModelState.IsValid)
            //    return BadRequest(ModelState);

            if(!TryValidateModel(dto))
                return BadRequest(ModelState);

            this.mapper.Map<PutDealer, Dealer>(dto, dealer);

            if (!await this.dealerRepository.SaveAsync(cancellationToken))
                return StatusCode(500, Message.STATUS_CODE_500_GENERAL_MESSAGE);

            return NoContent();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ProblemDetails))]
        public async Task<IActionResult> DeleteDealer(int id, CancellationToken cancellationToken)
        {
            var dealer = await this.dealerRepository.GetOneByIdAsync(id, cancellationToken);

            if (dealer == null)
                return NotFound();

            await this.dealerRepository.DeleteByIdAsync(dealer);

            if (!await this.dealerRepository.SaveAsync(cancellationToken))
                return StatusCode(500, Message.STATUS_CODE_500_GENERAL_MESSAGE);

            return NoContent();

        }
    }
}