﻿using AutoMapper;
using CarDealer.Api;
using CarDealer.Domain.Interfaces;
using CarDealer.Domain.Models.CarDealer;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Message = CarDealer.SeedWork.Api.Constants;

namespace CarDealer.Controllers
{
    [Route("/api/dealer/{dealerId}/car")]
    //[Route("[Controller]")]
    public class CarController : ControllerBase
    {
        private readonly IDealerRepository dealerRepository;
        private readonly IMapper mapper;
        private readonly ICarRepository carRepository;

        public CarController(IDealerRepository dealerRepository, 
                             ICarRepository carRepository,
                             IMapper mapper)
        {
            this.mapper = mapper;
            this.dealerRepository = dealerRepository;
            this.carRepository = carRepository;
        }

        [HttpGet("")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(List<CarResponse>))]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ProblemDetails))]
        public async Task<IActionResult> GetAllCarsByDealer(int dealerId, CancellationToken cancellationToken)
        {
            if (!await this.dealerRepository.ExistsAsync(dealerId, cancellationToken))
                return BadRequest($"Dealer {dealerId} no exists");

            var dealerCars = await this.carRepository.GetAllCarsByDealerId(dealerId, cancellationToken);

            return Ok(this.mapper.Map<List<CarResponse>>(dealerCars));
        }

        [HttpGet("{carId}", Name = "GetOneCarByDealer")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(CarResponse))]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ProblemDetails))]
        public async Task<IActionResult> GetOneCarByDealer(int dealerId, int carId, CancellationToken cancellationToken)
        {
            if (!await this.dealerRepository.ExistsAsync(dealerId, cancellationToken))
                return BadRequest($"Dealer {dealerId} no exists");

            var car = await this.carRepository.GetOneByDealerIdAndCarIdAsync(dealerId, carId, cancellationToken);

            if(car == null)
                return NotFound($"No Cars found for dealer {dealerId}");

            return Ok(this.mapper.Map<CarResponse>(car));
        }

        [HttpPost("")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ProblemDetails))]
        public async Task<IActionResult> CreateNewCarFromDealer(int dealerId, [FromBody]NewCar car, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (!await this.dealerRepository.ExistsAsync(dealerId, cancellationToken))
                return BadRequest($"Dealer {dealerId} no exists");

            var newCar = new Car(dealerId)
            {
                Name = car.Name
            };

            await this.carRepository.CreateAsync(newCar, cancellationToken);

            if (!await this.carRepository.SaveAsync(cancellationToken))
                return StatusCode(500, Message.STATUS_CODE_500_GENERAL_MESSAGE);

            return CreatedAtRoute("GetOneCarByDealer", new { dealerId = dealerId, carId = newCar.Id }, newCar);
        }

        [HttpPut("{carId}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ProblemDetails))]
        public async Task<IActionResult> UpdateCarFromDealer(int dealerId, int carId, [FromBody]UpdateCar car, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (!await this.dealerRepository.ExistsAsync(dealerId, cancellationToken))
                return BadRequest($"Dealer {dealerId} no exists");
            
            var oldCar = await this.carRepository.GetOneByDealerIdAndCarIdAsync(dealerId, carId, cancellationToken);

            if (oldCar == null)
                return NotFound($"Car {carId} found for dealer {dealerId}");

            this.mapper.Map<UpdateCar, Car>(car, oldCar);

            if (!await this.carRepository.SaveAsync(cancellationToken))
                return StatusCode(500, Message.STATUS_CODE_500_GENERAL_MESSAGE);

            return NoContent();
        }

        [HttpPatch("{carId}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ProblemDetails))]
        public async Task<IActionResult> UpdateCarFromDealer(int dealerId, int carId, [FromBody]JsonPatchDocument<UpdateCar> patchCarDocument, CancellationToken cancellationToken)
        {
            if (!await this.dealerRepository.ExistsAsync(dealerId, cancellationToken))
                return BadRequest($"Dealer {dealerId} no exists");

            var car = await this.carRepository.GetOneByDealerIdAndCarIdAsync(dealerId, carId, cancellationToken);

            if (car == null)
                return NotFound($"Car {carId} found for dealer {dealerId}");

            var carDto = this.mapper.Map<UpdateCar>(car);
            patchCarDocument.ApplyTo(carDto);

            if (!this.TryValidateModel(carDto))
                return BadRequest(ModelState);

            this.mapper.Map<UpdateCar, Car>(carDto, car);

            if (!await this.carRepository.SaveAsync(cancellationToken))
                return StatusCode(500, Message.STATUS_CODE_500_GENERAL_MESSAGE);

            return NoContent();
        }

        [HttpDelete("{carId}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(ProblemDetails))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ProblemDetails))]
        public async Task<IActionResult> DeleteCarFromDealer(int dealerId, int carId, CancellationToken cancellationToken)
        {
            if (!await this.dealerRepository.ExistsAsync(dealerId, cancellationToken))
                return BadRequest($"Dealer {dealerId} no exists");

            var car = await this.carRepository.GetOneByDealerIdAndCarIdAsync(dealerId, carId, cancellationToken);

            if (car == null)
                return NotFound($"Car {carId} found for dealer {dealerId}");

            await this.carRepository.DeleteByIdAsync(car);

            if (!await this.carRepository.SaveAsync(cancellationToken))
                return StatusCode(500, Message.STATUS_CODE_500_GENERAL_MESSAGE);

            return NoContent();
        }
    }
}