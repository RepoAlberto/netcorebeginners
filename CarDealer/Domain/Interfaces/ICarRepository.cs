﻿using CarDealer.Domain.Models.CarDealer;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CarDealer.Domain.Interfaces
{
    public interface ICarRepository : IGeneralRepository<Car>
    {
        Task<List<Car>> GetAllCarsByDealerId(int dealerId, CancellationToken cancellationToken);
        Task<Car> GetOneByDealerIdAndCarIdAsync(int dealerId, int carId, CancellationToken cancellationToken);
    }
}
