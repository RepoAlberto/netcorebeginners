﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CarDealer.Domain.Interfaces
{
    public interface IGeneralRepository<T> where T : class
    {
        Task<T> GetOneByIdAsync(int id, CancellationToken cancellationToken);
        Task<List<T>> GetAllPaginatedAsync(int take, int skip, CancellationToken cancellationToken);
        Task CreateAsync(T item, CancellationToken cancellationToken);
        Task DeleteByIdAsync(T id);
        Task<bool> SaveAsync(CancellationToken cancellation);
    }
}
