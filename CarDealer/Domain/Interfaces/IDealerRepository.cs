﻿using CarDealer.Domain.Models.CarDealer;
using System.Threading;
using System.Threading.Tasks;

namespace CarDealer.Domain.Interfaces
{
    public interface IDealerRepository : IGeneralRepository<Dealer>
    {
        Task<bool> ExistsAsync(int id, CancellationToken cancelationToken = default);
    }
}
