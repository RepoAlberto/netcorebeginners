﻿using CarDealer.SeedWork.Infraestructure;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarDealer.Domain.Models.CarDealer
{
    public class Color
    {
        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(Constants.NAME_DEFAULT_LENGTH, ErrorMessage = "Name of the dealer exceeds from maximun length")]
        public string Name { get; set; }
        
        [MaxLength(7, ErrorMessage = "Color is represented as hexadecimal. Example: #RRGGBB")]
        public string HexadecimalColor { get; set; }
    }
}