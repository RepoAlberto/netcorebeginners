﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarDealer.Domain.Models.CarDealer
{
    public class CarColor
    {
        [ForeignKey("CarId")]
        public Car Car { get; set; }
        public int CarId { get; set; }


        [ForeignKey("ColorId")]
        public Color Color { get; set; }
        public int ColorId { get; set; }
    }
}