﻿using CarDealer.SeedWork.Infraestructure;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarDealer.Domain.Models.CarDealer
{
    public class Dealer
    {
        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(Constants.NAME_DEFAULT_LENGTH, ErrorMessage = "Name of the dealer exceeds from maximun length")]
        public string Name { get; set; }

        [EmailAddress(ErrorMessage = "Email is not in the correct format. Example: mydealer@dealer.es")]
        public string Email { get; set; }

        [MaxLength(5, ErrorMessage = "Postal code exceed from max length")]
        public string PostalCode { get; set; }

        [Range(1, 5, ErrorMessage = "Rating must be between 1 and 5.")]
        public int Rating { get; set; }

        public List<Car> Cars { get; set; }
    }
}
