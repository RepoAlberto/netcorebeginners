﻿using CarDealer.SeedWork.Infraestructure;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarDealer.Domain.Models.CarDealer
{
    public class Car
    {
        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(Constants.MAX_DEFAULT_LENGTH), Required]
        public string Name { get; set; }

        public int Wheels { get; set; }

        [ForeignKey("DealerId")]
        public Dealer Dealer { get; private set; }
        public int DealerId { get; private set; }

        private Car()
        { }

        public Car(int dealerId)
        {
            this.DealerId = dealerId;
        }
    }
}