﻿using CarDealer.Domain.Models.CarDealer;
using Microsoft.EntityFrameworkCore;

namespace CarDealer.Infraestructure
{
    public class CarDealerDbContext: DbContext
    {
        public CarDealerDbContext(DbContextOptions<CarDealerDbContext> options) : base(options)
        { }

        public DbSet<Dealer> Dealers { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<CarColor> CarColors { get; set; }
        public DbSet<Color> Colors { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<CarColor>()
                .HasKey(ky => new { ky.CarId, ky.ColorId });

            modelBuilder
                .Entity<CarColor>()
                .HasIndex(indx => new { indx.CarId, indx.ColorId })
                .IsUnique();

            base.OnModelCreating(modelBuilder);
        }
    }
}