﻿using CarDealer.Domain.Interfaces;
using CarDealer.Domain.Models.CarDealer;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CarDealer.Infraestructure.Repositories
{
    public class DealerRepository : IDealerRepository
    {
        private readonly CarDealerDbContext dbContext;

        public DealerRepository(CarDealerDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task CreateAsync(Dealer dealer, CancellationToken cancellationToken) =>
            await this.dbContext.Dealers.AddAsync(dealer, cancellationToken);

        public async Task DeleteByIdAsync(Dealer dealer)
        {
            this.dbContext.Dealers.Attach(dealer);
            this.dbContext.Dealers.Remove(dealer);
        }

        public async Task<bool> ExistsAsync(int id, CancellationToken cancelationToken = default) =>
            await this.dbContext.Dealers.AnyAsync(e => e.Id == id, cancelationToken);

        public async Task<List<Dealer>> GetAllPaginatedAsync(int take, int skip, CancellationToken cancellationToken) =>
            await this.dbContext.Dealers.Skip(skip).Take(take).ToListAsync(cancellationToken);

        public async Task<Dealer> GetOneByIdAsync(int id, CancellationToken cancellationToken) =>
            await this.dbContext.Dealers.FirstOrDefaultAsync(e => e.Id == id, cancellationToken);

        public async Task<bool> SaveAsync(CancellationToken cancellation) => 
            await this.dbContext.SaveChangesAsync(cancellation) > 0;
    }
}