﻿using CarDealer.Domain.Interfaces;
using CarDealer.Domain.Models.CarDealer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CarDealer.Infraestructure.Repositories
{
    public class CarRepository : ICarRepository
    {
        private readonly CarDealerDbContext carDealerDbContext;

        public CarRepository(CarDealerDbContext carDealerDbContext)
        {
            this.carDealerDbContext = carDealerDbContext;
        }

        public async Task CreateAsync(Car item, CancellationToken cancellationToken) =>
            await this.carDealerDbContext.Cars.AddAsync(item, cancellationToken);

        public async Task DeleteByIdAsync(Car car)
        {
            this.carDealerDbContext.Cars.Attach(car);
            this.carDealerDbContext.Cars.Remove(car);
        }

        public async Task<List<Car>> GetAllCarsByDealerId(int dealerId, CancellationToken cancellationToken) =>
            await this.carDealerDbContext.Cars.Where(e => e.DealerId == dealerId).ToListAsync(cancellationToken);

        public Task<List<Car>> GetAllPaginatedAsync(int take, int skip, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<Car> GetOneByIdAsync(int id, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task<Car> GetOneByDealerIdAndCarIdAsync(int dealerId, int carId, CancellationToken cancellationToken) =>
            await this.carDealerDbContext.Cars.FirstOrDefaultAsync(e => e.DealerId == dealerId && e.Id == carId, cancellationToken);

        public async Task<bool> SaveAsync(CancellationToken cancellation) =>
            await this.carDealerDbContext.SaveChangesAsync(cancellation) > 0;
    }
}