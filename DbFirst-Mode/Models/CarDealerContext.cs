﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DbFirst_Mode.Models
{
    public partial class CarDealerContext : DbContext
    {
        public CarDealerContext()
        {
        }

        public CarDealerContext(DbContextOptions<CarDealerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CarColors> CarColors { get; set; }
        public virtual DbSet<Cars> Cars { get; set; }
        public virtual DbSet<Colors> Colors { get; set; }
        public virtual DbSet<Dealers> Dealers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=CarDealer;Integrated Security=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CarColors>(entity =>
            {
                entity.HasKey(e => new { e.CarId, e.ColorId });

                entity.HasIndex(e => e.ColorId);

                entity.HasIndex(e => new { e.CarId, e.ColorId })
                    .IsUnique();

                entity.HasOne(d => d.Car)
                    .WithMany(p => p.CarColors)
                    .HasForeignKey(d => d.CarId);

                entity.HasOne(d => d.Color)
                    .WithMany(p => p.CarColors)
                    .HasForeignKey(d => d.ColorId);
            });

            modelBuilder.Entity<Cars>(entity =>
            {
                entity.HasIndex(e => e.DealerId);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.HasOne(d => d.Dealer)
                    .WithMany(p => p.Cars)
                    .HasForeignKey(d => d.DealerId);
            });

            modelBuilder.Entity<Colors>(entity =>
            {
                entity.Property(e => e.HexadecimalColor).HasMaxLength(7);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Dealers>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.PostalCode).HasMaxLength(5);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
