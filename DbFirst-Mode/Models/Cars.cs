﻿using System;
using System.Collections.Generic;

namespace DbFirst_Mode.Models
{
    public partial class Cars
    {
        public Cars()
        {
            CarColors = new HashSet<CarColors>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int DealerId { get; set; }
        public int Wheels { get; set; }

        public virtual Dealers Dealer { get; set; }
        public virtual ICollection<CarColors> CarColors { get; set; }
    }
}
