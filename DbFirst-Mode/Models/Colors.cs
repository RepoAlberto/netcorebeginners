﻿using System;
using System.Collections.Generic;

namespace DbFirst_Mode.Models
{
    public partial class Colors
    {
        public Colors()
        {
            CarColors = new HashSet<CarColors>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string HexadecimalColor { get; set; }

        public virtual ICollection<CarColors> CarColors { get; set; }
    }
}
