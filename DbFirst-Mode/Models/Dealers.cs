﻿using System;
using System.Collections.Generic;

namespace DbFirst_Mode.Models
{
    public partial class Dealers
    {
        public Dealers()
        {
            Cars = new HashSet<Cars>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PostalCode { get; set; }
        public int Rating { get; set; }

        public virtual ICollection<Cars> Cars { get; set; }
    }
}
