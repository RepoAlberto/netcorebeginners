﻿using System;
using System.Collections.Generic;

namespace DbFirst_Mode.Models
{
    public partial class CarColors
    {
        public int CarId { get; set; }
        public int ColorId { get; set; }

        public virtual Cars Car { get; set; }
        public virtual Colors Color { get; set; }
    }
}
